
#this script actually does more than get a sound,
#but don't tell anyone. It's a secret.

#from the translate saved page

#get the chinese
chinese=$(cat Google\ Translate.html | tr "\n\r" "\t" | grep -Po 'id="source"[^>]*>\K([^<]*)')
#get the pinyin
pinyin=$(cat Google\ Translate.html | tr "\n\r" "\t" | grep -Po 'class="tlid-transliteration-content transliteration-content full">\K(.*?)(?=</div>)')
#get the english
english=$(cat Google\ Translate.html | tr "\n\r" "\t" | grep -Po 'class="tlid-translation translation" lang="en">[^>]*?>\K([^<]*)')

printf "\n=== cA === recieved request for new card:"
printf "\n=== chinese: $chinese"
printf "\n=== pinyin: $pinyin"
printf "\n=== english: $english\n"

#wait until other no other download is taking place.
while ! mkdir ./download.lock 2> /dev/null
do
    printf "\n=== cA === waiting to download $chinese\n"
    #wait 30 seconds between checking if download lock is released
    sleep 30
done

# if we already downloaded the audio for this lets not create the same card again.
if ls ~/"Documents/Anki/User 1/collection.media/$chinese.mp3"
then
  printf "\n=== cA === already got $chinese\n"
  rmdir ./download.lock
else

  ## stuff we will need for audio download request.
  url_encoded_search_string=$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$chinese")
  data_string="type=tns&spd=2&vol=9&per=2&tex=$url_encoded_search_string"

  #attempt downloading audio for 5 min... soo long! If failed, retry a couple times before giving up.
  printf "\n=== cA === downloading audio"
  printf "\n=== $chinese"
  printf "\n=== $pinyin"
  printf "\n=== $english\n"
  curl --connect-timeout 60 \
      --retry 5 \
      'https://cloud.baidu.com/aidemo' -H 'Host: cloud.baidu.com' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.9) Gecko/20100101 Goanna/3.4 Firefox/52.9 PaleMoon/27.6.2' -H 'Accept: */*' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' -H 'X-Requested-With: XMLHttpRequest' -H 'Referer: https://cloud.baidu.com/product/speech/tts' -H 'Cookie: BAIDUID=64D343D8E4DFEB669410A3F89148E8CF:FG=1; Hm_lvt_28a17f66627d87f1d046eae152a1c93d=1542471979; Hm_lpvt_28a17f66627d87f1d046eae152a1c93d=1542471979; BAIDU_CLOUD_TRACK_PATH=https://cloud.baidu.com/product/speech/tts' -H 'Connection: keep-alive' -H 'Pragma: no-cache' -H 'Cache-Control: no-cache' --data "$data_string" > sound_response

  sleep 5
  rmdir ./download.lock

  # turn that audio response into an audio file!
  cat sound_response | grep -Po 'base64,\K([^"]*)' | tr -d "\\" | base64 --decode > "$chinese.mp3"
  #move audio file to anki's media directory. We could probably do this through ankiconnect, but we're already running on this system, so this is easier!
  mv "$chinese.mp3" ~/Documents/Anki/User\ 1/collection.media/

  # put it all in anki using ankiconnect! Thanks Yomi-Chan! Thanks Alex Yatskov! Thank you to everyone at FooSoft Productions!
  curl localhost:8765 -X POST -d "
  {
      \"action\": \"addNote\",
      \"version\": 6,
      \"params\": {
          \"note\": {
              \"deckName\": \"三体\",
              \"modelName\": \"Basic\",
              \"fields\": {
                  \"Front\": \"$chinese\",
                  \"Back\": \"$pinyin<br>[sound:$chinese.mp3]<br>$english\"
              },
              \"options\": {
                  \"allowDuplicate\": false
              },
              \"tags\": [
                  \"ankifiedChinese\"
              ]
          }
      }
  }"

  printf "\n=== cA === Yeah! We got the card for $chinese !\n"

fi

